<?php
namespace App\Services;
use App\Services\DatabaseMovies;

class Movies 
{
    /**
     * 
     */
    private $movies;

    /**
     * @var DatabaseMovies
     */
     private $databaseMovies;

    public function __construct(DatabaseMovies $databaseMovies){
       $this->databaseMovies=$databaseMovies;
    }
   
    public function getMovies(){
       $this->jsonProcessing();  
       return $this->movies;
    }
    public function jsonProcessing(){
        $jsonMovies=$this->databaseMovies->getJsonMovies();
        $this->movies=json_decode($jsonMovies);
        
     }
}