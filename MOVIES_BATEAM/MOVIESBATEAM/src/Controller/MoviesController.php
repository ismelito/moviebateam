<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\Movies;

class MoviesController extends AbstractController
{
    /**
     * @var Movies
     */
    private $movies;

    public function __construct(Movies $movies){
       $this->movies=$movies;
    }
    
    
    public function list(): Response
    {   
        $movies=$this->movies->getMovies();
        return new JsonResponse($movies);
       /* $jsonMovies=$this->jsonMovies()
        //$movies= json_decode();
        try{
           
            return new JsonResponse($movies);
        }catch(Exception $e){
            return $this->json($e->getMessage()) ;
        }*/
        
    }
    public function jsonMovies(){
      
          
        return ;
    }
}
